import { createContext, useState } from "react";

const AuthContext = createContext({
  token: '',
  isLoggedIn: false,
  login: (token) => {},
  logout: () => {}
});

export const AuthContextProvider = (props) => {

    const [token, setToken] = useState(null)
    const userIsLoggednIn = !!token
    const loginHandler = (token) =>
    {
        setToken(token)
    }
    const logoutHandler = () => {
        setToken(null)
    }
    const contextValue = {
        token: token,
        isLoggedIn: userIsLoggednIn,
        login: loginHandler,
        logout: logoutHandler
    }
    return <AuthContextProvider value={contextValue}>{props.children}</AuthContextProvider>
}

export default AuthContext;
