import Table from "./components/UI/ListUrls";
import "./App.css";
import axios from "axios";
import NavBar from "./components/layout/navbar";
import SignupPage from "./components/UI/Signup.js";
import LoginPage from "./components/UI/Login.js";
import SendUrlPage from "./components/UI/ScanUrl.js";
import React, { useState, useMemo, useEffect, createContext } from "react";
export const LoginContext = createContext({
  isLogin: true,
  setIsLogin: () => {},
});

function App() {
  const [isLogin, setIsLogin] = useState(false);
  const [signupModalShow, setSignupModalShow] = useState(false);
  const [loginModalShow, setLoginModalShow] = useState(false);
  const [urlModalShow, setUrlModalShow] = useState(false);
  const [list, setList] = useState([]);
  useEffect(() => {
    axios.get("http://localhost:11801/history/show").then((res) => {
      setList(res.data);
    });
  }, []);
  useEffect(() => {
    axios.get("http://localhost:11801/isconnect").then((res) => {
      console.log(res)
      if(res.data === true){
        setIsLogin(true)
      }
    });
  }, []);
  const value = useMemo(() => ({ isLogin, setIsLogin }), [isLogin]);
  return (
    <LoginContext.Provider value={value}>
      <NavBar
        send={setList}
        onClickUrl={() => setUrlModalShow(true)}
        onClickSign={() => setSignupModalShow(true)}
        onClickLogin={() => setLoginModalShow(true)}
      />
      <SignupPage
        show={signupModalShow}
        onHide={() => setSignupModalShow(false)}
      />
      <LoginPage
        send={setList}
        show={loginModalShow}
        onHide={() => setLoginModalShow(false)}
      />
      <SendUrlPage
        list={list}
        send={setList}
        show={urlModalShow}
        onHide={() => setUrlModalShow(false)}
      />
      {(list.length !== 0) && <Table list={list} editList={setList}/>}
    </LoginContext.Provider>
  );
}

export default App;
