import React, { Fragment, useContext } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Nav, Navbar, Container, Button } from "react-bootstrap";
import {LoginContext} from "../../App"
import axios from "axios";
import { FcPlus } from "react-icons/fc";


const NavBar = (props) => {
  const {isLogin, setIsLogin} = useContext(LoginContext)
  const handlerLogout = (event) =>
  {
    event.preventDefault();
    axios.get('http://localhost:11801/auth/logout')
      .then(res => {
        console.log(res)
        if(res.data === true)
        {
          props.send([])
          setIsLogin(false)
        }
      })
      .catch((error) => {
        console.log(error)
        setIsLogin(true)
      })    
  }
  return (
      <Navbar bg="dark" variant="dark">
        <Container>
          <Navbar.Brand href="#home">Phishing Detect</Navbar.Brand>
          <Nav className="me-auto">
            {(isLogin) && <Nav.Link onClick={props.onClickUrl}><FcPlus/> Scan new Url</Nav.Link>}
            {(isLogin) && <Nav.Link>{<Button size="sm" active onClick={handlerLogout} variant="danger">Logout</Button>}</Nav.Link>}
            {(!isLogin) && <Nav.Link>{<Button size="sm" active onClick={props.onClickSign} variant="secondary">Sign Up</Button>}</Nav.Link>}
           { (!isLogin) && <Nav.Link>{<Button size="sm" active onClick={props.onClickLogin} variant="secondary">Login</Button>}</Nav.Link>}


          </Nav>
        </Container>
      </Navbar>

  );
};

export default NavBar;
