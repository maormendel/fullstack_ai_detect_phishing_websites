import { Button, Modal, Form, Alert } from "react-bootstrap";
import axios from "axios";
import React, { useContext, useState } from "react";
import { LoginContext } from "../../App";
import { SpinnerInfinity } from "spinners-react";
const Scanurl = (props) => {
  const { isLogin, setIsLogin } = useContext(LoginContext);
  const [show, setShow] = useState(false);
  const [isLoading, setIsloading] = useState(false);

  const closeOnClick = (event) => {
    event.preventDefault();
    props.onHide();
    setShow(false);
  };
  const submitFormHandler = (event) => {
    event.preventDefault();
    const details = {
      url: event.target.url.value,
      isPhishing: false,
      description: "-",
    };
    setIsloading(true);
    axios
      .post("http://localhost:11801/checkurl", details)
      .then((res) => {
        var list = props.list;
        console.log("a: ", list[0]);
        list.push(res.data);
        props.send(list);
        props.onHide();
        setIsloading(false);
        setShow(false);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <div>
      <Modal
        {...props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">Send Url</Modal.Title>
        </Modal.Header>
        {show && <Alert variant={"secondary"}>Send url failed</Alert>}
        <Modal.Body>
          <Form onSubmit={submitFormHandler}>
            <Form.Group className="mb-3" controlId="url">
              <Form.Label>Url</Form.Label>
              <Form.Control type="url" placeholder="Enter Url" />
            </Form.Group>
            {!isLoading ? (
              <Button variant="primary" type="submit">
                Submit
              </Button>
            ) : (
              <SpinnerInfinity
                size={50}
                thickness={100}
                speed={100}
                color="rgba(57, 77, 172, 1)"
                secondaryColor="rgba(0, 0, 0, 0.44)"
              />
            )}
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={closeOnClick}>Close</Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};
export default Scanurl;
