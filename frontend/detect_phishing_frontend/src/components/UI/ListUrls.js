import axios from "axios";
import React, { useState, useEffect } from "react";
import { Table, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import EditUrlPage from "./EditUrl"
const TableList = (props) => {
  const [show, setShow] = useState(false)
  const [obj, setObj] = useState({})
  const handleSubmit = props =>
  {
    setObj(props)
    setShow(true)
  }
  const handleRemoveSubmit = (props,list, editList) => 
  {
    console.log(props)
    const { _id} = props
    console.log(_id)
    axios.delete(`http://localhost:11801/history/delete/${_id}`)
    .then(res => {
      console.log(res)
      if (res.data === true){
        var result = list.filter(item => {
          return item._id !== _id
        })
        editList(result)
      }
    })
    .catch(err => {console.log(err)})
  }
  var rows = props.list.map((item, i) => {
    item.list = props.list
    item.editList = props.editList
    return (
      <tr key={i}>
        <td>{i}</td>
        <td>{item.url}</td>
        <td>{item.isPhishing.toString()}</td>
        <td>{item.text === undefined ? "-" : item.text}</td>
        <td><span><Button onClick={()=> {handleSubmit(item)}}>Edit</Button></span>{' '}<span><Button variant="danger" onClick={()=> {handleRemoveSubmit(item, props.list, props.editList)}}>Remove</Button></span></td>
      </tr>
    );
  });
  return (
    <div>
      <EditUrlPage show={show} onHide={() => setShow(false)} data={obj}/>
      <Table striped bordered hover variant="dark">
        <thead>
          <tr>
            <th>#</th>
            <th>Url</th>
            <th>is Phishing</th>
            <th>Description</th>
            <th>Edit</th>
          </tr>
        </thead>
        <tbody>{rows}</tbody>
      </Table>
    </div>
  );
};
export default TableList;
