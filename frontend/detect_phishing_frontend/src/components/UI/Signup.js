import { Button, Modal, Form, Alert} from "react-bootstrap";
import axios from 'axios'
import React, {useContext, useState} from 'react'
import { LoginContext } from "../../App";
const Signup = (props) => {
  const {isLogin, setIsLogin} = useContext(LoginContext)
  const [show, setShow] = useState(false);


  const closeOnClick = (event) =>
  {
    event.preventDefault()
    props.onHide()
    setShow(false)
  }
  const submitFormHandler = (event) => {
    event.preventDefault();
    const { password, username, email } = event.target
    const datails =
    {
      "username": username.value,
      "password": password.value,
      "email": email.value

    }
    axios.post('http://localhost:11801/auth/register', datails)
      .then(res => {
        if(res.data === true)
        {
            setIsLogin(true)
            props.onHide()
        }
        else
        {
            setShow(true)
        }
      })
      .catch((error) => {
        setShow(true)
        console.log(error)
      })
  };
  
  return (
    <div>
      <Modal
        {...props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Sign Up
          </Modal.Title>
        </Modal.Header>
        {show && <Alert variant={'secondary'}>Signup failed</Alert>}
        <Modal.Body>
        <Form onSubmit={submitFormHandler}>
              <Form.Group className="mb-3" controlId="email">
                <Form.Label>Email address</Form.Label>
                <Form.Control type="email" placeholder="Enter email" />
              </Form.Group>

              <Form.Group className="mb-3" controlId="username">
                <Form.Label>Username</Form.Label>
                <Form.Control type="username" placeholder="Enter Username" />
              </Form.Group>

              <Form.Group className="mb-3" controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control type="password" placeholder="Password" />
              </Form.Group>

              <Button variant="primary" type="submit">
                Submit
              </Button>
            </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={closeOnClick}>Close</Button>
        </Modal.Footer>
      </Modal>
      </div>
  );
};
export default Signup;
