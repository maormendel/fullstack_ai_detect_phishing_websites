import { Button, Modal, Form, Alert} from "react-bootstrap";
import axios from 'axios'
import React, {useEffect, useContext, useState} from 'react'
import { LoginContext } from "../../App";
const Login = (props) => {

  const {isLogin, setIsLogin} = useContext(LoginContext)
  const [show, setShow] = useState(false);
  const close = (func1, func2) => 
  {
    func1()
    func2(false)
  }
  const closeOnClick = (event) =>
  {
    event.preventDefault()
    close(props.onHide, setShow)

  }
  const submitFormHandler = (event) => {
    event.preventDefault();
    const { password, username } = event.target
    const datails =
    {
      "username": username.value,
      "password": password.value,
    }
    axios.post('http://localhost:11801/auth/login', datails)
      .then(res => {
        if(res.data === true)
        {
              axios.get("http://localhost:11801/history/show").then((res) => {
                props.send(res.data);})

            setIsLogin(true)
            props.onHide()
        }
        else
        {
            setShow(true)
        }
      })
      .catch((error) => {
        setShow(true)
        console.log(error)
      })
  };
  
  return (
    <div>
      <Modal 
        {...props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Login
          </Modal.Title>
        </Modal.Header>
        {show && <Alert variant={'secondary'}>Login failed</Alert>}
        <Modal.Body>
        <Form onSubmit={submitFormHandler}>
              <Form.Group className="mb-3" controlId="username">
                <Form.Label>Username</Form.Label>
                <Form.Control type="username" placeholder="Enter Username" />
              </Form.Group>

              <Form.Group className="mb-3" controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control type="password" placeholder="Password" />
              </Form.Group>

              <Button variant="primary" type="submit">
                Submit
              </Button>
            </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={closeOnClick}>Close</Button>
        </Modal.Footer>
      </Modal>
      </div>
  );
};

export default Login;
