import { Button, Modal, Form, Alert} from "react-bootstrap";
import axios from 'axios'
import React, {useEffect, useContext, useState} from 'react'
import { LoginContext } from "../../App";

const EditUrl = (props) => {
  const [show, setShow] = useState(false);
  const closeOnClick = (event) =>
  {
    event.preventDefault()
    props.onHide()
    setShow(false)
  }
  const submitFormHandler = (event) => {
    event.preventDefault();
    setShow(false)
    var foundIndex = props.data.list.findIndex(item => item._id === props.data._id);
    const jsonToSend = {
      text: event.target.description.value,
      isPhishing: event.target.box.checked,
      url: props.data.list[foundIndex].url
    }
    axios.put(`http://localhost:11801/history/${props.data._id}/edit`,jsonToSend)
    .then(res => {
      if(res.data === true)
      {
        props.data.list[foundIndex].text =  event.target.description.value
        props.data.list[foundIndex].isPhishing =  event.target.box.checked
        props.data.editList(props.data.list)
        props.onHide()
        setShow(false)
      }
    })
    .catch(err => {setShow(true)})

  };
  return (
    <div>
      <Modal
        {...props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Edit
          </Modal.Title>
        </Modal.Header>
        {show && <Alert variant={'secondary'}>Edit Failed</Alert>}
        <Modal.Body>
        <Form onSubmit={submitFormHandler}>
              <Form.Group className="mb-3" controlId="description">
                <Form.Label>description</Form.Label>
                <Form.Control type="description" defaultValue={props.data.text} placeholder="Enter description" />
              </Form.Group>

              <Form.Check
                inline
                enabaled
                defaultChecked={props.data.isPhishing} 
                label="phishing?"
                type={'checkbox'}
                id={`box`}
              />
              <Button variant="primary" type="submit">
                Submit
              </Button>
            </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={closeOnClick}>Close</Button>
        </Modal.Footer>
      </Modal>
      </div>
  );
};

export default EditUrl;
