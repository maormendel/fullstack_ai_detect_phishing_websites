import numpy as np
import pandas as pd
import tensorflow as tf
import sys
tf.__version__
def main(sample):
    new_model = tf.keras.models.load_model('my_model')
    print(new_model.predict(sample) > 0.5)
if __name__ == "__main__":
    main()
