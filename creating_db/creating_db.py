from bs4 import BeautifulSoup
import requests
import json
import sys
import numpy as np
import pandas as pd
import tensorflow as tf
tf.__version__

def helper(number_str):
    if ord(number_str) < 256 and ord(number_str) < 0:
        return True
    return False
class create_sample:
    def __init__(self, file_path):
        pass
    def make_dataset(self):
        count_1_have = 0.0
        count_1_all = 0.0
        count_2_have = 0.0
        count_2_all = 0.0
        table = []
        for index, row in self.df.iterrows():
            obj = []
            #length total
            obj.append(self.get_length_total(row[0]))
            #length domain
            obj.append(self.get_length_domain(row[0]))
            #num of dots
            obj.append(self.get_num_dots(row[0]))
            #get special characters
            obj.append(self.get_if_special_characters(row[0]))
            #has numbers
            obj.append(self.has_numbers(row[0]))
            #has SSL
            obj.append(self.has_SSL(row[0]))
            #has input
            res1 = self.parse_page(row[0], 'input')
            count_1_all += 1
            if res1 != None:
                count_1_have += 1
            obj.append(res1)
            #has alert
            res2 = self.parse_page(row[0], 'alert')
            count_2_all += 1
            if res2 != None:
                count_2_have += 1
            obj.append(res2)
            #virus total score:
            #obj.append(self.virus_total_check(row[0]))
            #page rank
            obj.append(row[-2])
            if row[-1] == "phishing":
                obj.append(1)
            else:
                obj.append(0)

            table.append(obj)


        return table

    def get_length_total(self, frame):
        return len(frame)

    def get_length_domain(self , frame):
        return len(frame.split('//')[1].split('/')[0])

    def get_num_dots(self, frame):
        return frame.count('.')
    def get_if_special_characters(self, frame):
        if (frame.count('@') + frame.count('#') + frame.count('%') + frame.count('$')) > 0:
            return 1
        return 0
    def has_numbers(self, frame):
        domain = frame.split('//')[1].split('/')[0]
        if any(char.isdigit() for char in domain) :
            return 1
        return 0
    def has_SSL(self, frame):
        if frame[0:5] == "https":
            return 1
        return 0
    def parse_page(self, frame, check):
        try:
            r = requests.get(frame)
            if(r.status_code == 200):
                soup = BeautifulSoup(r.content, 'html.parser')
                value = soup.find_all(check)
                if len(value) > 0:
                    return True
                return False
            return None
        except:
            return None
    
    def page_rank_score(self, frame):
        headers = {'API-OPR': 'socgcwowkggc8ss08okksgoww8408o44oss48ccw'}
        rank_url = "https://openpagerank.com/api/v1.0/getPageRank?domains[0]=" + frame
        response = requests.get(rank_url, headers=headers)
    def virus_total_check(self, frame):
        api_url = "https://www.virustotal.com/vtapi/v2/url/report?apikey=11cabce5c375a1c089fbede27c7f9d87771bfc1328dacfda6941d5780168d8ed&resource=" + frame
        params = dict(apikey=' 11cabce5c375a1c089fbede27c7f9d87771bfc1328dacfda6941d5780168d8ed',url=frame)
        response = requests.get(api_url)
        json_data = json.loads(response.content)
        if response.status_code == 200 and json_data["response_code"] == 1:
            return json_data["positives"]
        return -1

        #print(json.dumps(result, sort_keys=False, indent=4)
    def has_ip(frame):
        hostname = frame.split('//')[1].split('/')[0]
        if hostname.count('.') == 3:
            numbers = hostname.split('.')
            if helper(numbers[0]) and helper(numbers[1]) and helper(numbers[2]) and helper(numbers[3]):
                return True
            return False
        return False


     

def main(url):
    create = create_sample("")
    obj = []
    #length total
    obj.append(create.get_length_total( url))
    #length domain
    obj.append(create.get_length_domain(url))
    #num of dots
    obj.append(create.get_num_dots(url))
    #get special characters
    obj.append(create.get_if_special_characters(url))
    #has numbers
    obj.append(create.has_numbers(url))
    #has SSL
    obj.append(create.has_SSL(url))
    #has input
    #res1 = create.parse_page(url, 'input')
   # obj.append(res1)
    #has alert
    #res2 = create.parse_page(url, 'alert')
    

    print(obj)
    new_model = tf.keras.models.load_model('my_model')
    print(new_model.predict([obj]) > 0.5)
    
    


if __name__ == "__main__":
    url = sys.argv[1]
    main(url)