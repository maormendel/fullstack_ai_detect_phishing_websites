const express = require("express");
const router = express.Router();
const catchAsync = require("../utils/catchAsync");
const { urlSchema } = require("../schemas.js"); //config to mongoDB
const ExpressError = require("../utils/ExpressError");
const Url = require("../models/url");
const { isLoggedIn, validateUrlModel, isAuthor } = require("../middleware");

// get all urls history checks
router.get(
  "/show",
  catchAsync(async (req, res) => {
    if(global.currUser === null || global.currUser._id === null)
    {
      res.send([])
      return;
    }
    const id = global.currUser._id
    var urls = await Url.find({}).populate('author')
    urls = urls.filter(url => {return id.toString() === url.author._id.toString()})

    res.send(urls);
  })
);


router.put(
  "/:id/edit",
  isLoggedIn,
  isAuthor,
  catchAsync(async (req, res) => {
    const { id } = req.params;
    console.log(req.data)
    const jsonToSend = {
      _id: id,
      url: req.body.url,
      isPhishing: req.body.isPhishing,
      text: req.body.text,
    };
    const urlObject = await Url.findByIdAndUpdate(id, jsonToSend)
    .then(() => res.send(true))
    .catch(() => res.send(false))
  })
);

//delete a specific
router.delete(
  "/delete/:id",
  isLoggedIn, isAuthor,
  catchAsync(async (req, res) => {
    const { id } = req.params;
    await Url.findByIdAndDelete(id);
    res.send(true);
  })
);

module.exports = router;
