const express = require('express')
const router = express.Router()
const catchAsync = require('../utils/catchAsync')
const passport = require('passport')
const User = require('../models/user.js')

router.post('/register', catchAsync(async (req, res) => {
    try {
        const { username, email, password } = req.body
        const user = new User({ email, username })
        const registeredUser = await User.register(user, password)
        req.login(registeredUser, err => {
            if (err) return next(err);
            global.currUser = registeredUser
            res.send(true);
        })
    }
    catch (e) {
        console.log(e)
        res.send("something did not work properly")
    }
}))

router.post('/login', passport.authenticate('local', { failureRedirect: 'http://localhost:11801/' })
    , (req, res) => {
        console.group("login worked")
        res.send(true)    })
router.get('/logout', (req, res) => {
    req.logout()
    global.currUser = null
    res.send(true)
})

module.exports = router
