
const { campgroundSchema, reviewSchema } = require('./schemas.js');
const ExpressError = require('./utils/ExpressError');
const Url = require('./models/url');

module.exports.validateUrlModel = (req, res, next) => {
    const { error } = urlSchema.validate(req.body);
    if (error) {
        const msg = error.details.map(el => el.message).join(',')
        res.send("did not work")
        throw new ExpressError(msg, 400)
        
    } else {
        next();
    }
}


module.exports.isLoggedIn = (req ,res ,next) =>
{
    if(global.currUser === null || global.currUser._id === null)
    {
      res.send(false)
      return;
    }
    next()
}

module.exports.isAuthor = async (req, res, next) => 
{
    const { id } = req.params;
    console.log("s: ", id)
    console.log("s2: ", global.currUser._id)

    const url = await Url.findById(id);
    console.log(url)
    if (!url.author.equals(global.currUser._id)) {
        return res.send(false);
    }
    next();
}