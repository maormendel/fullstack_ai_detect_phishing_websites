const Joi = require('joi')


module.exports.urlSchema = Joi.object(
    {
        urls: Joi.object({
            url: Joi.string().required(),
            isPhishing: Joi.boolean().required(),
            text: Joi.string()
        }).required()
    }
)