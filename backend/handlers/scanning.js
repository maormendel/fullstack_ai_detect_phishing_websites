//https://xss-game.appspot.com/level1/frame
const {PythonShell} = require("python-shell")
module.exports.scanning = async(options) => {
    const result = await new Promise((resolve, reject) => {
      PythonShell.run('creating_db.py', options, (err, results) => {
        if (err) return reject(err);
        const ans = results[0]
        return resolve(ans);
      });
    });
    return result;
  }
  