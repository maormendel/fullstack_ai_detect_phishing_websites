// require outsource libraries
const express = require("express");
const mongoose = require("mongoose");
//requires from the project
const Url = require("./models/url");
const catchAsync = require("./utils/catchAsync");
const ExpressError = require("./utils/ExpressError");
const session = require("express-session");
const cors = require("cors");
const passport = require("passport");
const LocalStrategy = require("passport-local");
const User = require("./models/user");
const {PythonShell} = require('python-shell');
const history = require("./routes/history");
const authentication = require("./routes/users");
const { isLoggedIn } = require("./middleware");
const scannerHandler = require("./handlers/scanning");
const { reduceEachTrailingCommentRange } = require("typescript");



let opPythonPath;
let opPath;
if (process.platform == "linux") {
  opPythonPath = "/usr/bin/python";
  opPath = "/home/ubadmin/Desktop/a/fullstack_ai_detect_phishing_websites/creating_db";
} else {
  opPythonPath =
    "C:\\Users\\A\\AppData\\Local\\Microsoft\\WindowsApps\\python.exe";
  opPath = "C:\\Users\\A\\Desktop\\Rafael\\Website__Vul_Scanner\\attacks";
}
const options = {
  mode: "text",
  pythonPath: opPythonPath,
  pythonOptions: ["-u"],
  scriptPath: opPath,
  args: null,
};



global.currUser = null



mongoose.connect("mongodb://localhost:27017/detect-phishing", {
  useNewUrlParser: true,
  //    useCreateIndex: true,
  useUnifiedTopology: true,
});
//connect to the mongoDB
const db = mongoose.connection;
//check for errors
db.on("error", console.error.bind(console, "connection error:"));
//open the connection
db.once("open", () => {
  console.log("Database connected");
});

//condig the express to a variable and set that it will parser the messages from the client
//as a JSON format
const app = express();
app.use(express.json());
app.use(cors());
const sessionConfig = {
  secret: "thisshouldbeabettersecret!",
  resave: false,
  saveUninitialized: true,
  cookie: {
    httpOnly: true,
    expires: Date.now() + 1000 * 60 * 60 * 24 * 7,
    maxAge: 1000 * 60 * 60 * 24 * 7,
  },
};

app.use(session(sessionConfig));

app.use(passport.initialize());
app.use(passport.session());
passport.use(new LocalStrategy(User.authenticate()));

passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

app.use(async (req, res, next) => {
  if(req.body.username){
    const author = await User.find({"username" :  req.body.username})
    global.currUser = author[0]
  }
  next();
});

//middleware - routes of the websites(subdomains/types of requests)
app.use("/history", history);
app.use("/auth", authentication);
app.get("/isconnect",async (req, res) => {
  if(global.currUser !== null && global.currUser._id !== null){
    res.send(true)
    return;
  }
  res.send(false)
})
app.post(
  "/checkurl",
  catchAsync(async (req, res) => {
    if (global.currUser !== null && global.currUser.username !== null && global.currUser._id !== null)
    {
      const urlItem = new Url(req.body);
      urlItem.author = global.currUser._id;


      /*send the url to parse it for their properties
      like length, unique chars, num of dots
      */
      options.args = [urlItem.url]
      let url = await scannerHandler.scanning(options)
      url = url.substring(1, url.length -1)
      url = url.split(', ').map(item => {
        return parseFloat(item.trim())
      })

      //send to the AI model and get the result

      //save on the DB
      await urlItem.save();
      //return the result
      var data = urlItem
      data['author'] = global.currUser._id
      res.send(data);
    } else {
      res.send(false, "error has occured")
    }
  })
);
//home route
app.get("/", (req, res) => {
  res.send("hello");
});
//check url route

app.all("*", (req, res, next) => {
  next(new ExpressError("Page not found", 404));
});

//chose a port that the server will listen to(a.k.a - run the server)
PORT = 11801;
app.listen(PORT, () => {
  console.log(`Serving on port ${PORT}`);
});
