const mongoose = require('mongoose')

const Schema = mongoose.Schema

const UrlSchema = new Schema({
    url: { type: String, required: true,  },
    isPhishing: { type: Boolean, required: true },
    text: { type: String },
    author: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    }
})



module.exports = mongoose.model('url', UrlSchema)
